#! /bin/bash
# getSubs.sh seasons

# Download simultanously all subtitles from specified seasons of Stargate
# Atlantis from addic7ed

function episodeFinishedCallback() {
    # episodeFinishedCallback season episode remainingRequestDir

    [[ $# -ne 2 ]] && echo "Usage : $0 <season> <remainingRequestDir>" >&2 && return
    seasonNr=$1
    remainingRequestDir=$2

    lockfile="$remainingRequestDir/$currentSeason.lock"
    remainingFile="$remainingRequestDir/$seasonNr.finished"
    message="    Season $seasonNr finished"

    lockfile $lockfile

    read remainingDls < $remainingFile
    ((remainingDls--))
    echo "$remainingDls" > $remainingFile

    # clean
    [[ $remainingDls -eq 0 ]] && echo $message && rm $remainingFile
    remainingSeasons=$(ls -1 "$remainingRequestDir" | wc -l)
    [[ $remainingSeasons -eq 0 ]] && rm -r "$remainingRequestDir"

    rm -f $lockfile
}

function getSubUrl() {
    # getSubLink season episode

    [[ $# -ne 2 ]] && echo "Usage : $0 <season> <episode>" >&2 && return
    seasonNr=$1
    episodeNr=$2

    data=$(mktemp)
    status=$(mktemp)

    website="http://www.addic7ed.com"
    path="/serie/Stargate%20Atlantis/${seasonNr}/${episodeNr}/1"
    wget -o "$status" -O - ${website}${path} > "$data"

    # Don't print anything if request failed, the URL otherwise
    $(grep -q "200 OK" "$status")
    if [ $? -ne 0 ]
    then
        echo "Ep $seasonNr $episodeNr doesn't exist" >&2
        rm "$data" "$status"
        return
    fi

    cat "$data" | tr -d "\n" | sed -r 's/Version/\nVersion/g' | sed -nr 's/.*Version DVDRip.* class=.buttonDownload. href=\"([^\"]*)\".*/\1/p' | head -n 1

    rm "$data" "$status"
}

function downloadSub() {
    # downloadSub season episode URL outputFileName

    [[ $# -ne 4 ]] && echo "Usage : $0 <season> <episode> <URL> <output>" >&2 \
        && return
    seasonNr=$1
    episodeNr=$2
    url="http://www.addic7ed.com"$3
    outputFileName="output/"$4

    mkdir -p "output"

    wget \
        --header='Referer: http://www.addic7ed.com/serie/Stargate%20Atlantis/'$seasonNr'/'$episodeNr'/addic7ed' \
        "$url" -O - > "$outputFileName" 2> /dev/null
}

function getDownloadedEpisodeName() {
    # getDownloadedEpisodeName season episode

    [[ $# -ne 2 ]] && echo "Usage : $0 <season> <episode>" >&2 && return
    seasonNr=$1
    episodeNr=$2

    [[ $episodeNr -lt 10 ]] && episodeNr=0$episodeNr

    videoName=$(ls -1 ~/.torrent_data/Stargate\ Atlantis\ Season\ 1\,\ 2\,\ 3\,\ 4\ \&\ 5\ +\ Extras\ DVDRip\ TSV/Season\ ${seasonNr} | grep -E " Episode ${episodeNr} - .*\.avi")

    echo $videoName
}

function downloadSubsForEpisode() {
    # downloadSubsForEpisode season episode remainingRequestDir

    [[ $# -ne 3 ]] && echo "Usage : $0 <season> <episode> <remainingRequestDir>" >&2 && return
    seasonNr=$1
    episodeNr=$2
    remainingRequestDir=$3

    url=$(getSubUrl $seasonNr $episodeNr)
    [[ -z "$url" ]] && episodeFinishedCallback $seasonNr $remainingRequestDir \
        && return

    videoName=$(getDownloadedEpisodeName $seasonNr $episodeNr)
    outputFileName=${videoName/.avi/.srt}
    downloadSub "$seasonNr" "$episodeNr" "$url" "$outputFileName"
    episodeFinishedCallback $seasonNr $remainingRequestDir
}


[[ $# -lt 1 ]] && echo "Usage : $0 <first season number> ..." >&2 && exit 1

requestedSeasons="$@"
remainingRequestDir=$(mktemp -d)

cd $(dirname $0)

for currentSeason in $requestedSeasons
do
    lastRequestStatus=ok
    currentEpisode=1

    echo "Season $currentSeason"

    lockfile "$remainingRequestDir/$currentSeason.lock"
    for currentEpisode in {1..30}
    do
        echo "   dl episode $currentEpisode"
        downloadSubsForEpisode $currentSeason $currentEpisode $remainingRequestDir &
        # Should not be here
        echo "$currentEpisode" > "$remainingRequestDir/$currentSeason.finished"
    done
    rm -f "$remainingRequestDir/$currentSeason.lock"

done

for currentSeason in $requestedSeasons
do
    for currentEpisode in {1..30}
    do
        wait
        echo -e "   Fin  "
    done
done

exit 0
