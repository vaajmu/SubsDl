#! /bin/bash
# getSubs.sh seasons

# Download simultanously all subtitles from specified seasons of Jag from
# addic7ed

function episodeFinishedCallback() {
    # episodeFinishedCallback season episode remainingRequestDir

    [[ $# -ne 2 ]] && echo "Usage : $0 <season> <remainingRequestDir>" >&2 && return
    seasonNr=$1
    remainingRequestDir=$2

    lockfile="$remainingRequestDir/$currentSeason.lock"
    remainingFile="$remainingRequestDir/$seasonNr.finished"
    message="    Season $seasonNr finished"

    lockfile $lockfile

    read remainingDls < $remainingFile
    ((remainingDls--))
    echo "$remainingDls" > $remainingFile

    # clean
    [[ $remainingDls -eq 0 ]] && echo $message && rm $remainingFile
    remainingSeasons=$(ls -1 "$remainingRequestDir" | wc -l)
    [[ $remainingSeasons -eq 0 ]] && rm -r "$remainingRequestDir"

    rm -f $lockfile
}

function getSubUrl() {
    # getSubLink season episode

    [[ $# -ne 2 ]] && echo "Usage : $0 <season> <episode>" >&2 && return
    seasonNr=$1
    episodeNr=$2

    data=$(mktemp)
    status=$(mktemp)

    website="http://www.addic7ed.com"
    path="/re_episode.php"
    args="ep=566-${seasonNr}x${episodeNr}"
    wget -o "$status" -O - ${website}${path}?${args} > "$data"

    # Don't print anything if request failed, the URL otherwise
    $(grep -q "200 OK" "$status")
    if [ $? -ne 0 ]
    then
        echo "Ep $seasonNr $episodeNr doesn't exist" >&2
        rm "$data" "$status"
        return
    fi

    # cat "$data" | tr -d "\n" | sed -r 's/Version/\nVersion/g' | sed -nr 's/.*Version SDTV.* class=.buttonDownload. href=\"([^\"]*)\".*/\1/p'

    cat "$data" | tr -d "\n" | sed -r 's/Version/\nVersion/g' | sed -nr 's/.*Version DVDRip.* class=.buttonDownload. href=\"([^\"]*)\".*/\1/p' | head -n 1

    rm "$data" "$status"
}

function downloadSub() {
    # downloadSub season episode URL outputFileName

    [[ $# -ne 4 ]] && echo "Usage : $0 <season> <episode> <URL> <output>" >&2 \
        && return
    seasonNr=$1
    episodeNr=$2
    url="http://www.addic7ed.com"$3
    outputFileName="output/"$4

    mkdir -p "output"

    wget \
        --header='Host: www.addic7ed.com' \
        --header='User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0' \
        --header='Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' \
        --header='Accept-Language: en-US,en;q=0.5' \
        --header='Accept-Encoding: gzip, deflate' \
        --header='DNT: 1' \
        --header='Referer: http://www.addic7ed.com/serie/JAG/'$seasonNr'/'$episodeNr'/addic7ed' \
        --header='Cookie: __utma=217824198.2074919528.1458400423.1461509175.1461510003.22; __utmz=217824198.1461509175.21.18.utmcsr=subtitleseeker.com|utmccn=(referral)|utmcmd=referral|utmcct=/Download-tv-61729/JAG%20S05E01-S; __utmb=217824198.6.10.1461510003; __utmt=1; PHPSESSID=ckieq3fkphbjbrrivap1kuf243; __utmc=217824198' \
        --header='Connection: keep-alive' \
        "$url" -O - > "$outputFileName" 2> /dev/null
}

function getDownloadedEpisodeName() {
    # getDownloadedEpisodeName season episode

    [[ $# -ne 2 ]] && echo "Usage : $0 <season> <episode>" >&2 && return
    seasonNr=$1
    episodeNr=$2

    [[ $seasonNr -lt 10 ]] && seasonNr=0$seasonNr
    [[ $episodeNr -lt 10 ]] && episodeNr=0$episodeNr

    videoName=$(ls -1 ~/.torrent_data/Jag/JAG\ -\ Season\ ${seasonNr} | grep -E " ..x${episodeNr}.*\.avi")

    # folder="~/.torrent_data/Jag/JAG\\ -\\ Season\\ ${seasonNr}"
    # video="*${seasonNr}x${episodeNr}*.avi"
    # videoName=$(ls ${folder}/${video})
    echo $videoName
}

function downloadSubsForEpisode() {
    # downloadSubsForEpisode season episode remainingRequestDir

    [[ $# -ne 3 ]] && echo "Usage : $0 <season> <episode> <remainingRequestDir>" >&2 && return
    seasonNr=$1
    episodeNr=$2
    remainingRequestDir=$3

    url=$(getSubUrl $seasonNr $episodeNr)
    [[ -z "$url" ]] && episodeFinishedCallback $seasonNr $remainingRequestDir \
        && return

    videoName=$(getDownloadedEpisodeName $seasonNr $episodeNr)
    outputFileName=${videoName/.avi/.srt}
    downloadSub "$seasonNr" "$episodeNr" "$url" "$outputFileName"
    episodeFinishedCallback $seasonNr $remainingRequestDir
}


[[ $# -lt 1 ]] && echo "Usage : $0 <first season number> ..." >&2 && exit 1

requestedSeasons="$@"
remainingRequestDir=$(mktemp -d)

cd $(dirname $0)

for currentSeason in $requestedSeasons
do
    lastRequestStatus=ok
    currentEpisode=1

    echo "Season $currentSeason"

    lockfile "$remainingRequestDir/$currentSeason.lock"
    for currentEpisode in {1..30}
    do
        echo "   dl episode $currentEpisode"
        downloadSubsForEpisode $currentSeason $currentEpisode $remainingRequestDir &
        # Should not be here
        echo "$currentEpisode" > "$remainingRequestDir/$currentSeason.finished"
    done
    rm -f "$remainingRequestDir/$currentSeason.lock"

done

for currentSeason in $requestedSeasons
do
    for currentEpisode in {1..30}
    do
        wait
        echo -e "   Fin  "
    done
done

exit 0
