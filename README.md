Project SubsDl

Download subtitles from addic7ed.com

Available with Jag and Stargate Atlantis in english.

Use one of the two scripts provided with the list of needed season as
arguments. The .srt files will be downloaded on a folder named "output".

Lockfiles are used to notify the user that download is done but this should be
tested as it may be unecessary with some "wait" commands well put.

To be used, the episodes have to exist on hard drive as the script will detect
the name of each episode to give it to the corresponding .srt file. Therefore,
if the subtitles are moved to the video folder, the player will use them
automatically.

To download another show's subtitles, getSGASubs.sh may be copied instead of
getJagSubs.sh as SGA is younger. The URLs must be adapted in getSubUrl and
downloadSub. Those URLs can be found using a browser inspector on addic7ed pages
and the firefox plugin "Live HTTP header" (which is useful to see the request
actually made and not the links provided by addic7ed). getDownloadedEpisodeName
refers to the video name so the path must be adapted too.

Concurrent executon should be safe, but it deserves to be checked.

